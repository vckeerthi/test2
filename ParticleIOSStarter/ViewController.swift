//
//  ViewController.swift
//  ParticleIOSStarter
//
//  Created by Parrot on 2019-06-29.
//  Copyright © 2019 Parrot. All rights reserved.
//

import UIKit
import Particle_SDK

class ViewController: UIViewController {

    @IBOutlet weak var timeElapsed: UILabel!
    @IBOutlet weak var slider: UISlider!
    
    @IBOutlet weak var mohammadTime: UILabel!
    let USERNAME = "c0741753@mylambton.ca"
    let PASSWORD = "Bhargavi@2510"
    let DEVICE_ID = "44002c000f47363333343437"
    var myPhoton : ParticleDevice?
 
    
    var isGameOver: Bool = false
    var counterTimer = Timer()
    var counter = 0
    
    var sliderValue: Double!
    override func viewDidLoad() {
        super.viewDidLoad()
        particleSetup()
        getDeviceFromCloud()
      
    }
    func startCounter(){
        counterTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(incrementCounter), userInfo: nil, repeats:true)
       }
    @objc func incrementCounter(){

     if(!isGameOver){
        counter += 1
        timeElapsed.text = "Time Elapsed: \(counter)"
        let funcArgs = [(self.counter)] as [Any]
        var task = myPhoton!.callFunction("levels", withArguments: funcArgs) { (resultCode : NSNumber?, error : Error?) -> Void in
                    if (error == nil) {
                            print("LEDs successfully turned on")
                        }
                }
        
        if (counter >= 20 ){
            isGameOver = true

        }
        }
    }
    func startmohammadCounter(){
        var countTimer = Timer.scheduledTimer(timeInterval: self.sliderValue!, target: self, selector: #selector(incrementMohammadCounter), userInfo: nil, repeats:true)
    }
    @objc func incrementMohammadCounter(){

        if(!isGameOver){
           
           counter += 1
           timeElapsed.text = "Time Elapsed: \(counter)"
           let funcArgs = [(self.counter)] as [Any]
           var task = myPhoton!.callFunction("levels", withArguments: funcArgs) { (resultCode : NSNumber?, error : Error?) -> Void in
                       if (error == nil) {
                               print("LEDs successfully turned on")
                           }
                   }

           if (counter >= 20 ){
               isGameOver = true

           }
           }
       }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }

    func particleSetup(){
        ParticleCloud.init()
        
             
               ParticleCloud.sharedInstance().login(withUser: self.USERNAME, password: self.PASSWORD) { (error:Error?) -> Void in
                   if (error != nil) {
                      
                       print("Wrong credentials or as! ParticleCompletionBlock no internet connectivity, please try again")
                      
                       print(error?.localizedDescription)
                   }
                   else {
                       print("Login success!")
                   }
               }
    }
    func getDeviceFromCloud() {
    ParticleCloud.sharedInstance().getDevice(self.DEVICE_ID) { (device:ParticleDevice?, error:Error?) in
            
            if (error != nil) {
                print("Could not get device")
                print(error?.localizedDescription)
                return
            }
            else {
                print("Got photon: \(device?.id)")
                self.myPhoton = device
            }
            
        }    }
    @IBAction func startPressed(_ sender: Any) {
        startCounter()
   
    }
            

      
        
    @IBAction func sliderAction(_ sender: UISlider) {
        let currentValue = Int(sender.value)
             mohammadTime.text = "\(currentValue)"
            
            timeElapsed.text = "timeElapsed:\((self.counter) - currentValue) "
             self.sliderValue = Double(currentValue)
//            if(self.sliderValue != 0){
//                       startmohammadCounter()
//                       }
//                       else{
//                           startCounter()
//                       }
              
          
         }
    
       
    
}

