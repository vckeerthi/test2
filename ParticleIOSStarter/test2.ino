// This #include statement was automatically added by the Particle IDE.
#include <InternetButton.h>
InternetButton b = InternetButton();

void setup() {
b.begin();
Particle.function("levels",happyface);

}

void loop() {

}
int happyface(String cmd){
    int scope = cmd.toInt();
    if(scope >= 0 && scope<= 5){
        b.ledOn(5,0,0,100);
        b.ledOn(7,0,0,100);
        b.ledOn(3,0,0,100);
        b.ledOn(2,0,0,100);
        b.ledOn(1,0,0,100);
        b.ledOn(11,0,0,100);
        b.ledOn(10,0,0,100);
        b.ledOn(9,0,0,100);
        return 5;
    }
    else if(scope >=6 && scope <=10){
        b.allLedsOff();
        b.ledOn(5,0,100,100);
        b.ledOn(7,0,100,100);
       
        b.ledOn(2,0,100,100);
        b.ledOn(1,0,100,100);
        b.ledOn(11,0,100,100);
        b.ledOn(10,0,100,100);
        
         return 9;
    }
    else if(scope >=11 && scope <=15){
         b.allLedsOff();
        b.ledOn(5,100,100,100);
        b.ledOn(7,100,100,100);
        b.ledOn(1,100,100,100);
        b.ledOn(11,100,100,100);
         return 15;
    }
    else if(scope >=16 &&scope<=18){
         b.allLedsOff();
         b.ledOn(5,100,100,0);
        b.ledOn(7,100,100,0);
        return 19;
    }
    else if(scope == 20){
        b.allLedsOff();
        b.ledOn(5,100,0,0);
        b.ledOn(7,100,0,0);
        b.ledOn(1,100,0,0);
        b.ledOn(11,100,0,0);
        return 20;
    }
    else {
        return -1;
    }
}
